﻿#include <iostream>
#include <string>

int main()
{
    auto stringExample = std::string("string example");

    auto stringLength = stringExample.length();

    std::cout << stringExample << "\n" << stringLength << "\n" << stringExample[0] << "\n" << stringExample[stringLength - 1];
}
